module ThermostatApp
  class DBStatsCalculator

    def initialize(thermostat)
      @thermostat = thermostat
    end

    def calculate
      %w(temperature humidity battery_charge).each_with_object(Hash.new(0.0)) do |attr, hash|
        hash[attr] = calculate_by(attr)
      end
    end
    
    private

    def calculate_by(column_name)
      select_sql = "ROUND(AVG(#{column_name}), 2) as avg, ROUND(MIN(#{column_name}), 2) as min, ROUND(MAX(#{column_name}), 2) as max"
      
      @thermostat.readings.select(select_sql).first.attributes.slice('avg', 'min', 'max')
    end

  end
end
