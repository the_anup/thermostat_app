module ThermostatApp
  class StatsAggregator

    def initialize(thermostat)
      @thermostat = thermostat
      @db_calculator = ThermostatApp::DBStatsCalculator.new(@thermostat)
      @redis_calculator = ThermostatApp::RedisStatsCalculator.new(@thermostat.id)
    end

    def aggregate
      result_1 = @db_calculator.calculate
      result_2 = @redis_calculator.calculate
      result = result_1.merge(result_2) do |k, v1, v2|
        {
          'avg' => ((v1['avg'] + v2['avg'])/2).round(2),
          'min' => [v1['min'], v2['min']].min.round(2),
          'max' => [v1['max'], v2['max']].max.round(2)
        }
      end
      
      RedisCache.safe_call { RedisCache.redis.set("thermostat:#{@thermostat.id}", result.to_json) }
      result
    end

    def fetch_or_aggregate
      RedisCache.safe_call do
        if RedisCache.redis.exists("thermostat:#{@thermostat.id}")
          JSON.parse(RedisCache.redis.get("thermostat:#{@thermostat.id}"))
        else
          # it will almost never go where as even when the job is processed
          # the stats controller should show the stats.
          aggregate
        end
      end
    end
  end
end
