module ThermostatApp
  class RedisStatsCalculator

    def initialize(thermostat_id)
      @thermostat_id = thermostat_id
    end
    
    def calculate
      hash = Hash.new { |h,k| h[k] = []}
      extract_from_redis do |temperature, humidity, battery_charge|
        hash[:temperature] << temperature.to_f
        hash[:humidity] << humidity.to_f
        hash[:battery_charge] << battery_charge.to_f
      end
      %w(temperature humidity battery_charge).each_with_object({}) do |attr, memo| 
        memo[attr] = {
            'avg' => avg(hash[attr.to_sym]).round(2),
            'min' => hash[attr.to_sym].min.round(2),
            'max' => hash[attr.to_sym].max.round(2)
        }
      end
    end

    private

    def extract_from_redis
      RedisCache.safe_call do
        RedisCache.redis.keys("reading:#{@thermostat_id}:*").each do |key|
          yield RedisCache.redis.hmget(key, 'temperature', 'humidity', 'battery_charge')
        end
      end
    end

    def avg(arr)
      arr.inject{ |sum, el| sum + el }.to_f / arr.size
    end

  end
end
