# Thermostat Application

Clone
``` 
git clone git@gitlab.com:the_anup/thermostat_app.git
```
Bundle
```
bundle install
```
Database
```
rake db:create
rake db:migrate
rake db:seed
```
Devlopment

```
rails s
sidekiq (in a new tab)
```

Access sidekiq UI at localhost:3000/sidekiq

## API

### Create Reading
Request Type: POST

```
 localhost:3000/api/v1/readings
```

Params

```
 household_token: <household token for a thermostat> required
 temperature:     <float value>                      required
 humidity:        <float value>                      required
 battery_charge   <float value>                      required
```
Response

```
{tracking_number: 1 } 200
 
```

### Get Reading
Request Type: GET

```
localhost:3000/api/v1/readings/<tracking_number>
```

Params

```
household_token: <household token for a thermostat> required
```

Response

```
{
  "temperature": 36.56,
  "humidity": 12.43,
  "battery_charge": 98.33
} 200
```

### GET stats

Request Type: GET

```
localhost:3000/api/v1/stats
```

Params

```
household_token: <household token for a thermostat> required
```

Response

```
{
  "temperature": {
  "avg": 34.91,
  "min": 12.34,
  "max": 98.43
  },
"humidity": {
  "avg": 31.59,
  "min": 12.43,
  "max": 98.43
 },
 "battery_charge": {
   "avg": 74.54,
   "min": 25.45,
   "max": 98.43
  }
} 200
```

### Testing

```
rspec spec
```
