FactoryBot.define do
  factory :reading do
    temperature { 25.12 }
    humidity { 12.34 }
    battery_charge { 15.56 }
  end
end
