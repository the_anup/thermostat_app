require 'rails_helper'

RSpec.describe ReadingCreatorJob, type: :job do
  let(:thermostat) { create(:thermostat) }
  let(:tracking_number) { 1 }
  let(:temperature) { 25.34 }
  let(:humidity) { 12.43 }
  let(:battery_charge) { 98.44 }

  describe 'create reading' do
    it "should process the job" do
      expect {
        ReadingCreatorJob.new.perform(thermostat.id, tracking_number, temperature, humidity, battery_charge)
        key = "reading:#{thermostat.id}:#{tracking_number}"
        expect(RedisCache.redis.exists(key)).to eq(false)
        expect(Reading.tracking_number(thermostat)).to eq(2)
      }.to change(Reading, :count).by(1)
    end
  end

end
