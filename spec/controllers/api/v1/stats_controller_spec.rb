require 'rails_helper'

RSpec.describe Api::V1::StatsController, type: :controller do
  
  before(:each) do
    @thermostat = create(:thermostat)
    create(:reading, thermostat: @thermostat, temperature: 25.45, humidity: 35.56, battery_charge: 45.23)
    create(:reading, thermostat: @thermostat, temperature: 28.45, humidity: 32.56, battery_charge: 44.23)
    RedisCache.safe_call do 
      RedisCache.redis.hmset("#{Reading.redis_key_prefix}:#{@thermostat.id}:3", 'temperature', 27.34, 'humidity', 18.36, 'battery_charge', 98.32)
      RedisCache.redis.hmset("#{Reading.redis_key_prefix}:#{@thermostat.id}:4", 'temperature', 23.34, 'humidity', 15.36, 'battery_charge', 92.36)
    end
  end
  
  describe '#GET index' do
    it "should render response with success message" do
      get :index, params: { household_token: @thermostat.household_token }
      resp = JSON.parse(response.body)
      expect(response.status).to eq(200)
      %w(temperature humidity battery_charge).each do |attr|
        expect(resp[attr].class).to eq(Hash)
        expect(resp[attr].keys).to eq(%w(avg min max))
        expect(resp[attr]).to_not be_blank
      end
    end
    
    it "should return 422 if household token is not passed or doesn't match" do
      get :index, params: { household_token: 'some other token' }
      expect(response.status).to eq(422)
    end
  end


end
