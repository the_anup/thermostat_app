require 'rails_helper'

RSpec.describe Api::V1::ReadingsController, type: :controller do
  
  before(:each) do
    @thermostat = create(:thermostat)
  end
  
  describe '#POST create' do
    let(:temperature) { 25.3453 }
    let(:humidity) { 12.4343 }
    let(:battery_charge) { 98.443 }

    describe 'household token presence' do
      it "should return 422 when household token is not passed" do
        post :create, params: { temperature: temperature }
        expect(response.status).to eq(422)
      end
    end
    
    describe 'validations' do
      it "should return error when even of temperature, humidity or battery_charge is blank" do
        post :create, params: { temperature: temperature, humidity: humidity, battery_charge: '', household_token: @thermostat.household_token }
        resp = JSON.parse(response.body)
        expect(response.status).to eq(422)
        expect(resp['error']).to_not be_blank
      end
    end

    describe 'success' do
      it "should return tracking number" do
        tracking_number = Reading.tracking_number(@thermostat)
        key = "reading:#{@thermostat.id}:#{tracking_number}"
        args = [key, 'temperature', temperature.round(2), 'humidity', humidity.round(2), 'battery_charge', battery_charge.round(2)]
        expect(RedisCache.redis).to receive(:hmset).with(*args)
        expect(ReadingCreatorJob).to receive(:perform_later).with(@thermostat.id, tracking_number, temperature.round(2), humidity.round(2), battery_charge.round(2))
        post :create, params: { temperature: temperature, humidity: humidity, battery_charge: battery_charge, household_token: @thermostat.household_token }
        resp = JSON.parse(response.body)
        expect(response.status).to eq(200)
        expect(resp['tracking_number']).to eq(1)
      end
    end

    describe 'aquire lock' do
      
      before(:each) do
        @redlock_client = double(Redlock::Client)
        allow(Redlock::Client).to receive(:new).with([RedisCache::REDIS_CONFIG['url']]).and_return(@redlock_client)
        allow(ReadingCreatorJob).to receive(:perform_later).and_return(nil)
      end

      it "should aquire lock" do
        expect(@redlock_client).to receive(:lock!).and_yield
        post :create, params: { temperature: temperature, humidity: humidity, battery_charge: battery_charge, household_token: @thermostat.household_token }
      end
      
      it "should increase tracking number by 1 and send success when a race condition happens" do
        allow(Reading).to receive(:tracking_number).with(@thermostat).and_return(1)
        allow(@redlock_client).to receive(:lock!).and_raise(Redlock::LockError)
        post :create, params: { temperature: temperature, humidity: humidity, battery_charge: battery_charge, household_token: @thermostat.household_token }
        expect(response.status).to eq(200)
        resp = JSON.parse(response.body)
        expect(resp['tracking_number']).to eq(2)
      end
    end
  end

  describe '#GET show' do
    let(:thermostat) { create(:thermostat) }
    let(:tracking_number) { 1 }

    it "should return reading from redis if the key exists" do
      RedisCache.safe_call do
        RedisCache.redis.hmset("#{Reading.redis_key_prefix}:#{thermostat.id}:#{tracking_number}", 'temperature', 27.34, 'humidity', 18.36, 'battery_charge', 98.32)
      end

      get :show, params: {id: tracking_number, household_token: thermostat.household_token}
      expect(response.status).to eq(200)
      resp = JSON.parse(response.body)
      expect(resp['temperature']).to eq('27.34')
      expect(resp['humidity']).to eq('18.36')
      expect(resp['battery_charge']).to eq('98.32')

      RedisCache.safe_call do
        RedisCache.redis.del("#{Reading.redis_key_prefix}:#{thermostat.id}:#{tracking_number}")
      end
    end

    it "should return reading from database if reading has be created" do
      reading = create(:reading, thermostat: thermostat, tracking_number: tracking_number)
      get :show, params: {id: tracking_number, household_token: thermostat.household_token}
      expect(response.status).to eq(200)
      resp = JSON.parse(response.body)
      expect(resp['temperature']).to eq(reading.temperature)
      expect(resp['humidity']).to eq(reading.humidity)
      expect(resp['battery_charge']).to eq(reading.battery_charge) 
    end

  end

end
