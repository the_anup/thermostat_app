require 'rails_helper'

RSpec.describe Reading, type: :model do
  
  describe 'associations' do
    it { should belong_to(:thermostat) }
  end

  describe 'validations' do
    it { should validate_presence_of(:thermostat_id) }
    it {should validate_presence_of(:temperature) }
    it {should validate_presence_of(:humidity) }
    it {should validate_presence_of(:battery_charge) }
    it { should validate_numericality_of(:tracking_number).only_integer.is_greater_than(0) }
    it { should validate_numericality_of(:temperature) }
    it { should validate_numericality_of(:humidity) }
    it { should validate_numericality_of(:battery_charge) }
  end

  describe 'generate_tracking_number' do
    let(:thermostat_1) {
      create(:thermostat)
    }
    let(:thermostat_2) {
      create(:thermostat, location: 'some other location')
    }
    it "should generate a tracking number that starts from 1" do
      reading_1 = create(:reading, thermostat: thermostat_1)
      expect(reading_1.tracking_number).to eq(1)
      reading_2 = create(:reading, thermostat: thermostat_1)
      expect(reading_2.tracking_number).to eq(2)
      
      another_reading_1 = create(:reading, thermostat: thermostat_2)
      expect(another_reading_1.tracking_number).to eq(1)      
      another_reading_2 = create(:reading, thermostat: thermostat_2)
      expect(another_reading_2.tracking_number).to eq(2)
    end

    it "should honour tracking number if its passed from outside" do
      reading = create(:reading, thermostat: thermostat_1, tracking_number: 25)
      expect(reading.tracking_number).to eq(25)
    end

  end

end
