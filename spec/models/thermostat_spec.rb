require 'rails_helper'

RSpec.describe Thermostat, type: :model do

  describe 'associations' do
    it { should have_many(:readings).dependent(:nullify) }
  end

  describe 'validations' do
    it { should validate_presence_of(:location) }
  end

  describe 'generate_household_token' do
    it "should generate a household token" do
      thermostat = create(:thermostat)
      expect(thermostat.household_token).not_to be_blank
    end
    
    it "should not generate a household token if its passed from outside" do
      token = 'sometoken'
      thermostat = create(:thermostat, household_token: token)
      expect(thermostat.household_token).to eq(token)
    end
  end

end
