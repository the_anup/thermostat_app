require 'rails_helper'

RSpec.describe ThermostatApp::RedisStatsCalculator  do
  let(:thermostat) { create(:thermostat) }
  
  before(:each) do
    RedisCache.safe_call do 
      RedisCache.redis.hmset("#{Reading.redis_key_prefix}:#{thermostat.id}:1", 'temperature', 27.34, 'humidity', 18.36, 'battery_charge', 98.32)
      RedisCache.redis.hmset("#{Reading.redis_key_prefix}:#{thermostat.id}:2", 'temperature', 23.34, 'humidity', 15.36, 'battery_charge', 92.36)
    end
  end

  describe 'calculate' do
    it "should calculate correctly" do
      expected_result = {
        "temperature"=>{ "avg"=>25.34, "min"=>23.34, "max"=>27.34 },
        "humidity"=>{ "avg"=>16.86, "min"=>15.36, "max"=>18.36 },
        "battery_charge"=>{ "avg"=>95.34, "min"=>92.36, "max"=>98.32 }
      }
      expect(described_class.new(thermostat.id).calculate).to eq(expected_result)
    end
  end
end
