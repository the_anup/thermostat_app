require 'rails_helper'

RSpec.describe ThermostatApp::StatsAggregator do
  
  let(:thermostat) { create(:thermostat) }
  let(:expected_db_result) {
    {
      "temperature"=> { "avg"=>26.95, "min"=>25.45, "max"=>28.45 },
      "humidity"=> { "avg"=>34.06, "min"=>32.56, "max"=>35.56 },
      "battery_charge"=> { "avg"=>44.73, "min"=>44.23, "max"=>45.23 }
    }
  }
  let(:expected_redis_result) {
    {
      "temperature"=>{ "avg"=>45.85, "min"=>15.36, "max"=>98.32 },
      "humidity"=>{ "avg"=>45.85, "min"=>15.36, "max"=>98.32 },
      "battery_charge"=>{ "avg"=>45.85, "min"=>15.36, "max"=>98.32 }
    }
  }
  let(:expected_aggregated_result) {
    {
      "temperature"=>{"avg"=>36.4, "min"=>15.36, "max"=>98.32},
      "humidity"=>{"avg"=>39.96, "min"=>15.36, "max"=>98.32},
      "battery_charge"=>{"avg"=>45.29, "min"=>15.36, "max"=>98.32}
    }
  }
  before :each do
    @aggregator = described_class.new(thermostat)
    allow(@aggregator.instance_variable_get(:@db_calculator)).to receive(:calculate).and_return(expected_db_result)
    allow(@aggregator.instance_variable_get(:@redis_calculator)).to receive(:calculate).and_return(expected_redis_result)
  end

  describe 'aggregator' do
    it "should aggregate results correctly" do
      expect(@aggregator.aggregate).to eq(expected_aggregated_result)
    end
  end
end
