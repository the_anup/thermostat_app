require 'rails_helper'

RSpec.describe ThermostatApp::DBStatsCalculator do

  let(:thermostat) { create(:thermostat) }

  before(:each) do
    create(:reading, thermostat: thermostat, temperature: 25.45, humidity: 35.56, battery_charge: 45.23)
    create(:reading, thermostat: thermostat, temperature: 28.45, humidity: 32.56, battery_charge: 44.23)
  end

  describe 'calculate' do
    it "should calculate the result correctly" do
      expected_result = {
        "temperature"=> { "avg"=>26.95, "min"=>25.45, "max"=>28.45 },
        "humidity"=> { "avg"=>34.06, "min"=>32.56, "max"=>35.56 },
        "battery_charge"=> { "avg"=>44.73, "min"=>44.23, "max"=>45.23 }
      }
      expect(described_class.new(thermostat).calculate).to eq(expected_result)
    end
  end
  
end
