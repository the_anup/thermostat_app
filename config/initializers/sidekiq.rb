require 'sidekiq'

Sidekiq.default_worker_options = { retry: 0 } 

Sidekiq.configure_client do |config|
  config.redis = { url: RedisCache::REDIS_CONFIG[Rails.env] }
end

Sidekiq.configure_server do |config|
  config.redis = { url: RedisCache::REDIS_CONFIG[Rails.env] } 
end
