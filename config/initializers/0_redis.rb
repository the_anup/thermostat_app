require 'redis'

Redis::Client::DEFAULTS[:timeout] = 30


class RedisCache
  REDIS_CONFIG = YAML.load(ERB.new(File.read('config/redis.yml')).result)[Rails.env]

  class << self
    def redis
      @redis ||= Redis.new(url: REDIS_CONFIG['url'], timeout: REDIS_CONFIG['timeout'])
    end

    def safe_call
      begin
        yield
      rescue Exception => e
        Rails.logger.error e.message
        Rails.logger.error e.backtrace.join("\n")
        return nil
      end
    end
    
  end
  
end
