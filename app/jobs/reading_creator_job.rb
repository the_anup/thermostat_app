class ReadingCreatorJob < ApplicationJob
  queue_as :default
  
  def perform(thermostat_id, tracking_number, temperature, humidity, battery_charge)
    Reading.create!(
        thermostat_id: thermostat_id,
        tracking_number: tracking_number,
        temperature: temperature,
        humidity: humidity,
        battery_charge: battery_charge
    )

    aggregate(thermostat_id)
    key = "reading:#{thermostat_id}:#{tracking_number}"
    RedisCache.safe_call { RedisCache.redis.del(key) }
  end

  def aggregate(thermostat_id)
    thermostat = Thermostat.find(thermostat_id)
    aggregator = ThermostatApp::StatsAggregator.new(thermostat)
    aggregator.aggregate
  end
end
