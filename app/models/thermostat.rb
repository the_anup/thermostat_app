class Thermostat < ApplicationRecord
  has_many :readings, dependent: :nullify

  before_create :generate_household_token

  validates :location, presence: true
  
  protected

  def generate_household_token
    if attributes['household_token'].nil?
      self.household_token = SecureRandom.hex
    end
  end
  
end
