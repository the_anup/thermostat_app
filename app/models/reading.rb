class Reading < ApplicationRecord
  REDIS_KEY_PREFIX = 'reading'.freeze
  
  belongs_to :thermostat

  before_create :generate_tracking_number
  
  validates :thermostat_id, presence: true
  validates :tracking_number, numericality: { only_integer: true, greater_than: 0 }, if: lambda { |obj| obj.attributes['tracking_number'].present? }
  validates :temperature, :humidity, :battery_charge, presence: true, numericality: true

  class << self
    def tracking_number(thermostat)
      (thermostat.readings.maximum(:tracking_number) || 0) + 1
    end
    
    def redis_key_prefix
      REDIS_KEY_PREFIX
    end
  end

  protected

  def generate_tracking_number
    if attributes['tracking_number'].nil?
      self.tracking_number = self.class.tracking_number(thermostat)
    end
  end
  
end
