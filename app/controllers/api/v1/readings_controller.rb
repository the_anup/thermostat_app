module Api
  module V1
    class ReadingsController < Api::BaseController

      REDLOCK_EXPIRATION_TIME = 2000

      before_action :validate_readings, only: [:create]
      before_action :validate_tracking_number, only: [:show]

      def create
        tracking_number = Reading.tracking_number(@thermostat)
        key = "#{Reading.redis_key_prefix}:#{@thermostat.id}:#{tracking_number}"
        begin
          redlock_client.lock!("resource_#{key}", REDLOCK_EXPIRATION_TIME) do
            RedisCache.safe_call { RedisCache.redis.hmset(*hmset_args(key)) }
            ReadingCreatorJob.perform_later(
              @thermostat.id, 
              tracking_number, 
              temperature, 
              humidity, 
              battery_charge
            )
            render json: { tracking_number: tracking_number }, status: :ok
            
          end
        rescue Redlock::LockError
          # if there is a race condition where multiple readings are
          # trying to created with same tracking number (it will happen if second reading comes before first is saved in the DB)
          # incrementing the tracking number by 1 is a safe way to not create that reading
          new_tracking_number = tracking_number + 1
          new_key = "#{Reading.redis_key_prefix}:#{@thermostat.id}:#{new_tracking_number}"
          RedisCache.safe_call { RedisCache.redis.hmset(*hmset_args(new_key)) }
          ReadingCreatorJob.perform_later(
            @thermostat.id, 
            new_tracking_number, 
            temperature, 
            humidity, 
            battery_charge
          )
          render json: { tracking_number: new_tracking_number }, status: :ok
        end
      end

      def show
        tracking_number = params[:id]
        key = "#{Reading.redis_key_prefix}:#{@thermostat.id}:#{tracking_number}"
        if RedisCache.safe_call { RedisCache.redis.exists(key) }
          temperature, humidity, battery_charge = RedisCache.safe_call { RedisCache.redis.hmget(*hmget_args(key)) }
          render json: { temperature: temperature, humidity: humidity, battery_charge: battery_charge }, status: :ok
        else
          reading = Reading.find_by_thermostat_id_and_tracking_number!(@thermostat.id, tracking_number)
          render json: { temperature: reading.temperature, humidity: reading.humidity, battery_charge: reading.battery_charge }, status: :ok
        end
      end

      private

      def validate_readings
        if params_blank?
          render json: { error: 'Temprature, Humidity and Battery Charge should not be blank'}, status: :unprocessable_entity and return
        end
      end

      def validate_tracking_number
        if params[:id].blank?
          render json: { error: 'Tracker number should not be blank'}, status: :unprocessable_entity and return
        end
      end

      def params_blank?
        params[:temperature].blank? || 
          params[:humidity].blank? || 
          params[:battery_charge].blank?
      end
      
      
      def hmset_args(key)
        [
          key,
          'temperature',
          temperature,
          'humidity',
          humidity,
          'battery_charge',
          battery_charge
        ]
      end

      def hmget_args(key)
        [
          key,
          'temperature',
          'humidity',
          'battery_charge'
        ]
      end

      def temperature
        params[:temperature].to_f.round(2)
      end

      def humidity
        params[:humidity].to_f.round(2)
      end

      def battery_charge
        params[:battery_charge].to_f.round(2)
      end
      
      def redlock_client
        Redlock::Client.new([RedisCache::REDIS_CONFIG['url']])
      end
      
    end
  end
end
