module Api
  module V1
    class StatsController < Api::BaseController
      
      def index
        aggregator = ThermostatApp::StatsAggregator.new(@thermostat)
        render json: aggregator.fetch_or_aggregate, status: :ok
      end
    end
  end
end
