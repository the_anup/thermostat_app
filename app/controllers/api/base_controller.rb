module Api
  class BaseController < ApplicationController

    rescue_from ActiveRecord::RecordInvalid, with: :render_unprocessable_entity_response
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response
    
    before_action :find_by_household_token

    protected

    def render_unprocessable_entity_response(exception)
      error =  exception.record.errors.collect do |attr, msg|
        "#{attr.to_s} #{msg}"
      end.join(",")
      render json: { error: error }, status: :unprocessable_entity
    end

    def render_not_found_response(exception)
      render json: { error: exception.message }, status: :not_found
    end

    def find_by_household_token
      @thermostat = Thermostat.find_by_household_token(params[:household_token])
      head :unprocessable_entity unless @thermostat
    end

  end
end
